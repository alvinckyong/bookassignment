var express = require("express");
var app = express();

var bodyParser = require("body-parser");
var mysql = require("mysql");

var pool = mysql.createPool({
    host: "localhost",
    port: 3306,
    user: "root",
    password: "12345",
    database: "library",
    connectionLimit: 4
});

const PORT = process.argv[2] || process.env.APP_PORT || 3000;
const GET_ALL_TITLES_WHERE_TITLE_LIKE_AS = "SELECT id, title, author_firstname, author_lastname, cover_thumbnail FROM books WHERE title like ? ORDER BY title LIMIT 10"
const GET_ALL_TITLES_WHERE_AUTHOR_LIKE_AS = "SELECT id, title, author_firstname, author_lastname, cover_thumbnail FROM books WHERE author_firstname like ? ORDER BY title LIMIT 10"
const GET_ALL_TITLES_WHERE_ID_IS_EQUAL = "SELECT id, title, author_firstname, author_lastname, cover_thumbnail FROM books WHERE id=?"
const UPDATE_TITLE_WHERE_ID_IS_EQUAL = "UPDATE title, author_firstname, author_lastname FROM books WHERE id=?"


app.get("/api/books/titles/:title_like", function(req, res){
    console.info(req);
    pool.getConnection(function(err, connection){
        if(err){
            //console.info();
            res.status(400).send(JSON.stringify(err));
            return;
        }
        var title_like = "%" + req.params.title_like + "%";
        connection.query(GET_ALL_TITLES_WHERE_TITLE_LIKE_AS,[title_like],function(err,results){
            connection.release();
            if(err){
                res.status(400).send(JSON.stringify(err));
                return;
            }
            console.info(results);
            res.json(results);
        });
    });
});

app.get("/api/books/authors/:author_like", function(req, res){
    pool.getConnection(function(err, connection){
        if(err){
            console.log("ERR: " + JSON.stringify(err));
            res.status(500).send(JSON.stringify(err));
            return;
        }
        var author_like = "%" + req.params.author_like + "%";
        connection.query(GET_ALL_TITLES_WHERE_AUTHOR_LIKE_AS,[author_like],function(err,results){
            connection.release();
            if(err){
                console.log("ERR: " + JSON.stringify(err));
                res.status(500).send(JSON.stringify(err));
                return;
            }
            console.info("result " + results);
            res.json(results).end();
        });
    });
});

app.get("/api/books/id/:bookId", function(req, res){
    pool.getConnection(function(err, connection){
        if(err){
            console.log("ERR: " + JSON.stringify(err));
            res.status(500).send(JSON.stringify(err));
            return;
        }
        console.log (req.params);
        connection.query(GET_ALL_TITLES_WHERE_ID_IS_EQUAL, [req.params.bookId], function(err,results){
            connection.release();
            if(err){
                console.log("ERR: " + JSON.stringify(err));
                res.status(500).send(JSON.stringify(err));
                return;
            }
            console.info("result " + JSON.stringify(results[0]));
            res.json(results).end();
        });
    });
});

app.post("/api/books/id/:bookId", function(req, res){
    pool.getConnection(function(err, connection){
        if(err){
            console.log("ERR: " + JSON.stringify(err));
            res.status(500).send(JSON.stringify(err));
            return;
        }
        console.log (req.params);
        connection.query(GET_ALL_TITLES_WHERE_ID_IS_EQUAL, [req.params.bookId], function(err,results){
            connection.release();
            if(err){
                console.log("ERR: " + JSON.stringify(err));
                res.status(500).send(JSON.stringify(err));
                return;
            }
            console.info("result " + JSON.stringify(results[0]));
            res.json(results).end();
        });
    });
});

app.use(express.static(__dirname + "/public"));
app.use("/bower_components", express.static(__dirname + "/bower_components"));

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.listen(PORT, function(){
    console.log("Server is running now", PORT)
});