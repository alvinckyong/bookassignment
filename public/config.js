(function(){
    angular
        .module("BookApp")
        .config(bookConfig);
    
    bookConfig.$inject = ["$stateProvider", "$urlRouterProvider"];
    
    function bookConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("edit", {
                url: "/edit/:bookId",
                templateUrl: "edit.html",
                controller: "UpdateCtrl",
                controllerAs: "ctrl"
            })

            .state("search", {
                url: "/",
                templateUrl: "search.html",
                controller: "searchCtrl",
                controllerAs: "ctrl"
            })

        $urlRouterProvider.otherwise('/');
    }
})();