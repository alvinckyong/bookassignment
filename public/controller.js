(function(){
    angular
        .module("BookApp")
        .controller("searchCtrl", searchCtrl)
        .controller("UpdateCtrl", UpdateCtrl);

    searchCtrl.$inject = ["dbService", "$state"]
    UpdateCtrl.$inject = ["dbService", "$stateParams", "$state"]
    
    function searchCtrl(dbService, $state) {
        var vm = this;
        vm.title = "";
        vm.author = "";
        vm.results = [];

        vm.searchByTitle = function () {
            dbService.searchByTitle(vm.title)
                .then(function (result) {
                    vm.results = result;
                    console.info("results: %s", JSON.stringify(vm.results));
                }).catch(function (error) {
                vm.err = error;
                console.info(">> error: %s", JSON.stringify(error));
            });
        };

        vm.searchByAuthor = function () {
            dbService.searchByAuthor(vm.author)
                .then(function (result) {
                    vm.results = result;
                    console.info(vm.results);
                }).catch(function (error) {
                vm.err = error;
            });
        };

        vm.edit = function (id){
            console.info("edit is called " + id);
            $state.go("edit", {'bookId': id});
        }
    }
        
        function UpdateCtrl(dbService, $state, $stateParams) {
            var vm = this;
            vm.book = {};
            console.log("hello");
            console.log($stateParams);

            dbService
                .searchByBookId($stateParams.params.bookId)
                .then(function (book) {
                    vm.book = book[0];
                });
        }
})();