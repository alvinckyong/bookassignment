(function () {
    angular
        .module("BookApp")
        .service("dbService", dbService);

    dbService.$inject = ["$http", "$q"];

    function dbService($http, $q) {
        var vm = this;

        vm.searchByTitle = function (title){
            var defer = $q.defer();
            $http.get("/api/books/titles/" + title)
                .then(function(result){
                    defer.resolve(result.data);
                }).catch(function(err){
                defer.reject(err);
            });
            return defer.promise;
        };

        vm.searchByAuthor = function (author){
            var defer = $q.defer();
            $http.get("/api/books/authors/" + author)
                .then(function(result){
                    defer.resolve(result.data);
                }).catch(function (err){
                    defer.reject(err);
            });
            return defer.promise;
        };

        vm.searchByBookId = function (bookId){
            var defer = $q.defer();
            $http.get("/api/books/id/" + bookId)
                .then(function(result){
                    defer.resolve(result.data);
                }).catch(function (err){
                defer.reject(err);
            });
            return defer.promise;
        };

        vm.save = function (bookId){
            $http.post("/api/books/id/" +bookId)
                .then(function () {
                    console.info("success");
                    vm.status.message = "The book is saved to the database.";
                    vm.status.code = 202;
                }).catch(function () {
                console.info("Error");
                vm.status.message = "Failed to save the book to the database.";
                vm.status.code = 400;
            });
        };

    }
})();

